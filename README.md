# Unsere Acht Planeten

Dieses Repository enthält die Quellen für eine mit dem [tiptoi-Stift] von
Ravensburger bespielbare Seite zu den Planeten unseres Sonnensystems. Das
Projekt entstand im Rahmen einer [Kinderbastelrunde] des [Hackspace Jena e.v.].

Die Quellen bestehen aus der Datei `planeten.xcf` für das
Bildbearbeitungsprogramm [GIMP] und der Datei `planeten.yaml` für das Werkzeug
[tttool], mit dessen Hilfe man eine GME-Datei für den tiptoi-Stift erstellen
kann.

[tiptoi-Stift]:
https://www.ravensburger.de/entdecken/ravensburger-marken/tiptoi/index.html
'Offizielle Webseite zum tiptoi-Stift'

[Kinderbastelrunde]:
https://kraut.space/hswiki:veranstaltungen:regelmaessige:kinderbasteln
'Kinderbastelrunde des Hackspace Jena e.V.'

[Hackspace Jena e.v.]:
https://kraut.space
'Hackspace Jena e.V.'

[GIMP]:
https://www.gimp.org/
'Offizielle Webseite zu GIMP'

[tttool]:
https://tttool.entropia.de
'Offizielle Webseite zu tttool'

## Grafik

Die Datei `planeten.xcf` ist eine Datei für das Bildbearbeitungsprogram [GIMP].
Die Größe in Pixeln (4659 x 6415) passt auf meinem Drucker bei 600x600dpi fast
genau in den bedruckbaren Bereich. Für andere Drucker muss das ggf. angepasst
werden.

Die Datei ist recht fein in Layer gegliedert, damit man verschiedene Varianten
erstellen und drucken kann.

Zum Testen bietet es sich an, statt des schwarzen Hintergrunds den weißen zu
verwenden, um Farbe zu sparen. Möchte man die Datei je einmal auf einem
Farbdrucker ohne Muster drucken, um auf einem Schwarz-Weiß-Drucker die Muster
darüber zu drucken, stellt man zunächst die Layergruppen Labelcodes und
Planetenlabelcodes auf nicht sichtbar und druckt dann die Muster auf dem zweiten
Drucker in dem man nur diese Layergruppen sichtbar schaltet.

Auf der Webseite von [tttool] finden sich weitere Hinweise, wie das Drucken
unter Umständen erfolgreich sein kann.

## Programm

Die Datei `planeten.yaml` beschreibt das tiptoi-Programm und kann mit Hilfe von
tttool in eine GME-Datei übersetzt werden. Die GME-Datei kann dann auf den
tiptoi-Stift kopiert werden.

Dem Projekt liegen keine Audiodateien bei, da wir die im Rahmen des Projektes
gesprochenen Texte nicht veröffentlichen wollen. Die Datei `planeten.yaml`
enthält jedoch Anweisungen (und damit Texte) um Audiodateien via Text-To-Speech
zu generieren.

Die Texte haben keine große Tiefe. Die Idee war, dass sie von 3 bis 4 Jahre
alten Kindern verstanden und gesprochen werden können. Wie sich herausstellte,
waren sie für einige Kinder immer noch zu schwer.

Für weitere Informationen zu tttool verweisen wir auf dessen [Webseite].

[Webseite]:
https://tttool.entropia.de
'Offizielle Webseite zu tttool'

## Icon

Die Datei `icon.svg` enhält das Logo des Projekts. Das Skript `build_icon.sh`
erzeugt aus `icon.svg` eine 200x200 Pixel große PNG-Datei. Dafür muss
[Inkscape] installiert sein.

[Inkscape]:
https://inkscape.org
Inkscapes Offizielle Webseite'

## Lizenzen

Dieses Projekt ist unter der 'Creative Commons
Attribution-NonCommercial-ShareAlike 3.0 Unported License' veröffentlicht. Eine
Kopie der Lizenz findet sich in der Datei LICENSE. Urheber aller Quellen ist,
soweit im folgenden nicht anders angegeben, Philipp Matthias Schäfer
<philipp.matthias.schaefer@posteo.de>.

### Schriftart

Der Text in der Grafik ist mit DejaVu Sans gesetzt. Deren Urheberrechtssituation
ist unter https://dejavu-fonts.github.io/License.html nachzuschlagen.

### Icons

Die drei Icons stammen von https://game-icons.net . Die folgende Liste gibt die
Lizenssituation jedes Icons an.

- 'Power button icon', Lord Berandas, CC-BY 3.0
- 'Chat bubble icon', Delapoutie, CC-BY 3.0
- 'Perspective dice 6 faces 1 icon', Delapoutie, CC-BY 3.0

### Bilder der Planeten

Die verwendeten Bilder der Planeten stammen aus verschiedenen Quellen. Die
folgende Liste gibt die Lizenzsituation jedes Quellbildes an.

- Merkur: 'Mercury in color from MESSENGER', NASA / JHUAPL / CIW / color mosaic
  by Jason Perry, CC-BY-NC-SA 3.0
- Venus: 'Global view of Venus from Mariner 10', NASA / JPL / Mattias Malmer,
  mit Erlaubnis für dieses Projekt
- Erde: 'Earth in true color', NASA Goddard Space Flight Center, Public Domain
- Mars: 'Late spring on Mars (centered on roughly 305 degrees longitude)', NASA,
  David Crisp and the WFPC2 Science Team (Jet Propulsion Laboratory/California
  Institute of Technology), Public Domain
- Jupiter: 'High Resolution Globe of Jupiter', NASA/JPL/University of Arizona,
  Public Domain
- Saturn: 'Saturn, Approaching Northern Summer', NASA/JPL-Caltech/Space Science
  Institute, Public Domain
- Uranus: 'Color global view of Uranus from Voyager 2', NASA / JPL / Björn
  Jónsson, CC-BY-NC-SA 3.0
- Neptun: 'Neptune from Voyager', NASA / JPL / color processing by Björn
  Jónsson, CC-BY-NC-SA 3.0
